#include <Encoder.h>
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#include <LiquidCrystalRus.h>


#define menu_items_count 4
#define DHTPIN 4
#define DHTTYPE    DHT11


Encoder myEnc(44, 45);
LiquidCrystalRus lcd(32, 50, 33, 34, 35, 36, 37);
DHT_Unified dht(DHTPIN, DHTTYPE);


long oldPosition  = -999;
long temp;


long getEncoder(){
	  long newPosition = myEnc.read() / 4;
	  if (newPosition != oldPosition) {
		oldPosition = newPosition;
		return newPosition;
	  } else {
		return -1;
	}
}

long normalizeEncoder(long position){
	if (position < 0)
		position *= -1;
	position %= menu_items_count;
	return position;
}

void menu(long position=getEncoder()){
	position = normalizeEncoder(position);
	Serial.println(position);
	switch (position)
	{
	case 0:
		{
			menu0();
			break;
		}

	case 1:
		{
			menu1();
			break;
		}

	case 2:
		{
			menu2();
			break;
		}

	case 3:
		{
			menu3();
			break;
		}

	default:
		break;
	}
}

void menu0(){
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Меню 0");
	while(true){
		temp = getEncoder();
		if (temp != -1){
			temp = normalizeEncoder(temp);
			break;
		}
	}
	menu(temp);
}

void menu1(){
	sensors_event_t event;
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Температура");
	while(true){
		long temperature_millis = 0;
		if (millis() - temperature_millis > 1500){
			lcd.setCursor(0, 1);
			dht.temperature().getEvent(&event);
			if (!isnan(event.temperature)) {
				lcd.print(event.temperature);
			}
			temperature_millis = millis();
		}
		temp = getEncoder();
		if (temp != -1){
			temp = normalizeEncoder(temp);
			break;
		}
	}
	menu(temp);
}

void menu2(){
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Меню 2");
	while(true){
		temp = getEncoder();
		if (temp != -1){
			temp = normalizeEncoder(temp);
			break;
		}
	}
	menu(temp);
}

void menu3(){
	lcd.clear();
	lcd.setCursor(0, 0);
	lcd.print("Меню 3");
	while(true){
		temp = getEncoder();
		if (temp != -1){
			temp = normalizeEncoder(temp);
			break;
		}
	}
	menu(temp);
}


void setup() {
	Serial.begin(115200);
	lcd.begin(16, 2);
	dht.begin();
	menu();
}

void loop() {

}