package com.ethernetapis.smartlamp

import android.graphics.drawable.ColorDrawable
import android.os.AsyncTask
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class MainActivity : AppCompatActivity(),OnSeekBarChangeListener {


    fun update(view: View){
        val sukatask = BluatTask()
        sukatask.execute()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<SeekBar>(R.id.seekBar_R).setOnSeekBarChangeListener(this)
        findViewById<SeekBar>(R.id.seekBar_G).setOnSeekBarChangeListener(this)
        findViewById<SeekBar>(R.id.seekBar_B).setOnSeekBarChangeListener(this)
        findViewById<SeekBar>(R.id.seekBar_A).setOnSeekBarChangeListener(this)
    }

    override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
        val suka_R = findViewById<SeekBar>(R.id.seekBar_R).getProgress()
        val suka_G = findViewById<SeekBar>(R.id.seekBar_G).getProgress()
        val suka_B = findViewById<SeekBar>(R.id.seekBar_B).getProgress()
        val suka_A = findViewById<SeekBar>(R.id.seekBar_A).getProgress()
        findViewById<View>(R.id.view).setForeground(ColorDrawable((suka_A*16777216 + suka_R*65536 + suka_G*256 + suka_B).toInt()))
    }
    override fun onStartTrackingTouch(seekBar: SeekBar) {
    }
    override fun onStopTrackingTouch(seekBar: SeekBar) {
    }


    internal inner class BluatTask : AsyncTask<Void, Void, Void>() {

        override fun onPreExecute() {
            super.onPreExecute()

        }

        override fun doInBackground(vararg params: Void): Void? {
            val suka_R = findViewById<SeekBar>(R.id.seekBar_R).getProgress()
            val suka_G = findViewById<SeekBar>(R.id.seekBar_G).getProgress()
            val suka_B = findViewById<SeekBar>(R.id.seekBar_B).getProgress()
            val suka_A = findViewById<SeekBar>(R.id.seekBar_A).getProgress()
            var urlConnection: HttpURLConnection
            var url = URL("http://185.172.129.20/index.php?color=" + (suka_A*16777216 + suka_R*65536 + suka_G*256 + suka_B).toUInt())
            urlConnection = url.openConnection() as HttpURLConnection
            var reader = BufferedReader(InputStreamReader(urlConnection.inputStream))
            return null
        }

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
        }
    }
}
