<?php
    include "../config/config.php";
    $mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
    if($mysqli->connect_errno){
        echo $mysql_error_msg;
        exit(0);
    }
    $userid = -1;
    if(isset($_COOKIE["UID"]))
        $userid = intval($_COOKIE["UID"]);
    if($userid != -1 && $mysqli->query("SELECT `authtoken` FROM `users` WHERE userid='$userid';")->fetch_object()->{"authtoken"} == sha1($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'])){
        $mysqli->query("UPDATE `users` SET `authtoken`=' ' WHERE userid='$userid';");
    }
    $mysqli->close();
    setcookie("UID", "", time() - 3600);
    header('Location: ' . $site_host);