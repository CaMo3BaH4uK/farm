<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include "../config/config.php";
	include "../config/bans.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>">
		<link rel="stylesheet" type="text/css" href="view.css" media="all">
		<script type="text/javascript" src="view.js"></script>
	</head>
	<body id="main_body" >
		<img id="top" src="top.png" alt="">
		<div id="form_container">
			<h1><a>Регистрация нового аккаунта</a></h1>
			<form id="form_93090" class="appnitro"  method="post" action="#">
				<div class="form_description">
					<h2>Регистрация нового аккаунта в системе мониторинга</h2>
					<p>Уже зарегистрированы? <a href="../login">Авторизация</a></p>
				</div>						
				<ul>
					<li id="li_1" >
						<label class="description" for="register_form_email">E-Mail </label>
						<div>
							<input required id="register_form_email" name="register_form_email" class="element text medium" type="text" maxlength="255" value="<?php if(isset($_POST['register_form_email'])) echo($_POST['register_form_email']); ?>" title="Введите верный E-mail" pattern="^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$"/> 
						</div><p class="guidelines" id="guide_1"><small>Введите E-Mail</small></p> 
					</li>		
					<li id="li_2" >
						<label class="description" for="register_form_password">Пароль </label>
						<div>
							<input required id="register_form_password" name="register_form_password" class="element text medium" type="password" maxlength="255" value="<?php if(isset($_POST['register_form_password'])) echo($_POST['register_form_password']); ?>" title="Введите пароль (от 8 символов)" pattern="[\S]{8,}"><button id="register_form_password_show" name="register_form_password_show" type="button">Показать пароль</button></input> 
						</div><p class="guidelines" id="guide_2"><small>Придумайте пароль</small></p> 
					</li>
					<li class="buttons">
						<input type="hidden" name="form_id" value="93090" />
						<input id="saveForm" class="button_text" type="submit" name="submit" value="Зарегистрироваться" />
					</li>
				</ul>
			</form>	
		</div>
		<img id="bottom" src="bottom.png" alt="">
		<?php
			include "../config/custom.php";
			$mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
			if($mysqli->connect_errno){
				echo $mysql_error_msg;
				exit(0);
			}
			$checker = 0;
			if(isset($_POST["register_form_email"]) && trim($_POST["register_form_email"]) != NULL && preg_match('/^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$/', trim($_POST["register_form_email"]))) { $checker++; $email = $mysqli->real_escape_string(trim($_POST["register_form_email"])); }
			if(isset($_POST["register_form_password"]) && trim($_POST["register_form_password"]) != NULL && preg_match('/[\S]{8,}/', trim($_POST["register_form_password"]))) { $checker++; $hashed_password = crypt($mysqli->real_escape_string(trim($_POST["register_form_password"]))); }
			if($checker == 2){
				if($mysqli->query("SELECT COUNT(*) FROM `users` WHERE email='$email';")->fetch_object()->{"COUNT(*)"} == "0"){
					$request = $mysqli->query("SELECT MAX(userid) FROM users")->fetch_object()->{"MAX(userid)"};
					if($request == NULL)
						$userid = 1;
					else
						$userid = intval($request) + 1;
					$authtoken = sha1($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR']);
					setcookie("UID", $userid, time()+60*60*24*30, "/");
					if($mysqli->query("INSERT INTO `users` (`userid`, `email`, `password`, `authtoken`) VALUES ('$userid', '$email', '$hashed_password', '$authtoken');")){
						phpPost_redirect('../panel/', array('type' => 'success', 'title' => 'Добро пожаловать', 'msg' => 'Вы успешно зарегистрировалиси в системе'));
					}else{
						phpAlert_engine('error', 'Системная ошибка', 'Ошибка при создании аккаунта');
					} 
				}else{
					phpAlert_engine('error', 'Ошибка', 'Данный E-mail уже занят');
				}
			}elseif($checker){
				phpAlert_engine('error', 'Предупреждение', 'Проверьте правильность ввода данных');
			}
			$mysqli->close();
		?>
		<script>
			$("#register_form_password_show").hover(function() {
				$("#register_form_password").attr("type", "text");
			}, function () {
				$("#register_form_password").attr("type", "password");
			});
		</script>
	</body>
</html>