<?php
    include "../config/userauth.php";
    $arr = $mysqli->query("SELECT * FROM `farms` WHERE userid='$userid';")->fetch_all(MYSQLI_ASSOC);
    $pres_arr = $mysqli->query("SELECT * FROM `presets` WHERE userid='$userid';")->fetch_all(MYSQLI_ASSOC); 
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://raw.githubusercontent.com/CodeSeven/toastr/master/toastr.js"></script>
        <style>
        * {box-sizing: border-box}

        /* Set height of body and the document to 100% */
        body, html {
        height: 100%;
        margin: 0;
        font-family: Arial;
        }

        /* Style tab links */
        .tablink {
        background-color: #555;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 25%;
        }

        .tablink:hover {
        background-color: #777;
        }

        /* Style the tab content (and add height:100% for full page content) */
        .tabcontent {
        color: white;
        display: none;
        padding: 100px 20px;
        height: 100%;
        }

        #Farms {background-color: green;}
        #Presets {background-color: green;}
        #Settings {background-color: green;}
        #Exit {background-color: green;}
        </style>
    </head>
    <body>
        <button class="tablink" onclick="openPage('Farms', this, 'green')" id="defaultOpen">Фермы</button>
        <button class="tablink" onclick="openPage('Presets', this, 'green')">Пресеты</button>
        <button class="tablink" onclick="openPage('Settings', this, 'green')">Настройки</button>
        <button class="tablink" onclick="window.location.href='<?php echo $site_host; ?>exit'">Выйти</button>

        <div id="Farms" class="tabcontent">
            <h3><a href="farms/register">Зарегистрировать новую ферму</a></h3>
            <table border="1">
                <caption>Список ваших ферм</caption>
                <tr>
                    <th>ID фермы</th>
                    <th>Название фермы</th>
                    <th>Текущий пресет</th>
                    <th>Температура</th>
                    <th>Влажность</th>
                    <th>Свет</th>
                    <th>Бак с водой</th>
                    <th>Последнее появление в сети</th>
                    <th>Токен</th>
                    <th>Действия</th>
                </tr>
                <?php
                    for($i = 0; $i < count($arr); $i++){
                        $preset_id = $arr[$i]["presetid"];
                        $presetname = $mysqli->query("SELECT `presetname` FROM `presets` WHERE presetid='$preset_id';")->fetch_object()->{"presetname"};
                        $preset = $presetname . " (ID пресета: " . $preset_id . ")";
                        echo "<tr><td>" . $arr[$i]["farmid"] . "</td><td>" . $arr[$i]["farmname"] . "</td><td>" . $preset . "</td><td>" . $arr[$i]["temperature"] . "</td><td>" . $arr[$i]["humidity"] . "</td><td>" . $arr[$i]["light"] . "</td><td>" . $arr[$i]["tank"] . "</td><td>" . $arr[$i]["lastonline"] . "</td><td>" . $arr[$i]["farmtoken"] . '</td><td><a href="farms/edit?farmid=' . $arr[$i]["farmid"] . '">Отредактировать</a>   <a href="farms/delete?farmid=' . $arr[$i]["farmid"] . '">Удалить</a></td></tr>';
                    }
                ?>
            </table>
        </div>

        <div id="Presets" class="tabcontent">
            <h3><a href="presets/register">Зарегистрировать новый пресет</a></h3>
            <table border="1">
                <caption>Список ваших пресетов</caption>
                <tr>
                    <th>ID пресета</th>
                    <th>Название пресета</th>
                    <th>Условие лампы</th>
                    <th>Условие помпы</th>
                    <th>Условие вентилятора</th>
                    <th>Условие светодиодной ленты</th>
                    <th>Действия</th>
                </tr>
                <?php
                    for($i = 0; $i < count($pres_arr); $i++){
                        if ($pres_arr[$i]["lamp"] != "off" && $pres_arr[$i]["lamp"] != 'on')
                            $lamp = "Условие";
                        else
                            $lamp = $pres_arr[$i]["lamp"];

                        if ($pres_arr[$i]["pump"] != "off" && $pres_arr[$i]["pump"] != 'on')
                            $pump = "Условие";
                        else
                            $pump = $pres_arr[$i]["pump"];

                        if ($pres_arr[$i]["fan"] != "off" && $pres_arr[$i]["fan"] != 'on')
                            $fan = "Условие";
                        else
                            $fan = $pres_arr[$i]["fan"];
                        echo "<tr><td>" . $pres_arr[$i]["presetid"] . "</td><td>" . $pres_arr[$i]["presetname"] . "</td><td>" . $lamp . "</td><td>" . $pump . "</td><td>" . $fan . "</td><td>" . $pres_arr[$i]["led"] . '</td><td><a href="presets/edit?presetid=' . $pres_arr[$i]["presetid"] . '">Отредактировать</a>   <a href="presets/delete?presetid=' . $pres_arr[$i]["presetid"] . '">Удалить</a></td></tr>';
                    }
                ?>
            </table>
        </div>

        <div id="Settings" class="tabcontent">
            <form id="form" method="post" action="#">
                <h3>Смена пароля:</h3>
                </br>
                Старый пароль: <input required id="edit_password_old" name="edit_password_old" type="password" size="30" value=""><button id="edit_password_old_show" name="edit_password_old_show" type="button">Показать пароль</button></input>
                </br>
                </br>
                Новый пароль: <input required id="edit_password_new" name="edit_password_new" type="password" size="30" value="" pattern="[\S]{8,}"><button id="edit_password_new_show" name="edit_password_new_show" type="button">Показать пароль</button></input>
                </br>
                </br>
                <input type="submit" name="button_submit" value="Изменить"/>
            </form>
        </div>

        <script>
            function openPage(pageName,elmnt,color) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablink");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].style.backgroundColor = "";
            }
            document.getElementById(pageName).style.display = "block";
            elmnt.style.backgroundColor = color;
            }

            document.getElementById("defaultOpen").click();

            $("#edit_password_old_show").hover(function() {
			    $("#edit_password_old").attr("type", "text");
            }, function () {
                $("#edit_password_old").attr("type", "password");
            });
            $("#edit_password_new_show").hover(function() {
                $("#edit_password_new").attr("type", "text");
            }, function () {
                $("#edit_password_new").attr("type", "password");
            });
        </script>
        <?php 
            include "../config/custom.php";
            $change_password = 0;
            if(isset($_POST["edit_password_old"]) && trim($_POST["edit_password_old"]) != NULL) { $change_password++; $password = $mysqli->real_escape_string(trim($_POST["edit_password_old"])); }
            if(isset($_POST["edit_password_new"]) && trim($_POST["edit_password_new"]) != NULL && preg_match('/[\S]{8,}/', trim($_POST["edit_password_new"]))) { $change_password++; $hashed_password_new = crypt($mysqli->real_escape_string(trim($_POST["edit_password_new"]))); }
            if($change_password == 2){
                $hashed_password = $mysqli->query("SELECT `password` FROM `users` WHERE userid='$userid';")->fetch_object()->{"password"};
                if (hash_equals($hashed_password, crypt($password, $hashed_password))){
                    $mysqli->query("UPDATE `users` SET `password`='$hashed_password_new' WHERE userid='$userid';");
                    phpAlert_engine("success","Операция завершена","Изменения пароля внесены успешно");
                }else{
                    phpAlert_engine("error","Ошибка ввода","Проверьте правильность ввода паролей");
                }
            }elseif($change_password){
                phpAlert_engine("error","Ошибка ввода","Для смены пароля заполните все значения");
            }
        ?>
    </body>
</html> 
<?php
    $mysqli->close();
?>