<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <?php
    include "../../../config/userauth.php";
    $presetid = -1;
	if(isset($_GET['presetid'])){
		$presetid = intval($mysqli->real_escape_string(trim($_GET['presetid'])));
	}else{
		header('Location: ' . $site_host . 'panel');
		exit(0);
    }
	$auth_check = false;
	if($presetid != -1 && intval($mysqli->query("SELECT `userid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"userid"}) == $userid){
        if(intval($mysqli->query("SELECT `farmid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"farmid"}) == -1){
            header('Location: ' . $site_host . 'panel/presets/edit?presetid=' . $presetid);
            $presetid = -1;
            exit(0);
        }else{
            $auth_check = true;
        }   
	}else{
		setcookie("UID", "", time() - 3600);
		$userid = -1;
		header('Location: ' . $site_host);
		exit(0);
    }
    $arr = $mysqli->query("SELECT * FROM `presets` WHERE presetid='$presetid';")->fetch_all(MYSQLI_ASSOC);
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Редактирование пресета</title>
    <link rel="stylesheet" type="text/css" href="view.css" media="all">
    <script type="text/javascript" src="view.js"></script>

</head>

<body id="main_body">

    <img id="top" src="top.png" alt="">
    <div id="form_container">

        <h1><a>Редактирование пресета</a></h1>
        <form id="form_93544" class="appnitro" method="post" action="">
            <div class="form_description">
                <h2>Редактирование пресета</h2>
                <p>Выберите состояние устройств<br><a href="../..">Вернуться</a></p>
            </div>
            <ul>

                <li id="li_1">
                    <label class="description" for="element_1">Fan </label>
                    <div>
                        <select class="element select medium" id="element_1" name="element_1">
                            <option value="off" <?php if($arr[0]["fan"] == "off") echo('selected="selected"'); ?> >Выключить</option>
                            <option value="on" <?php if($arr[0]["fan"] == "on") echo('selected="selected"'); ?> >Включить</option>
                        </select>
                    </div>
                </li>
                <li id="li_2">
                    <label class="description" for="element_2">Lamp </label>
                    <div>
                        <select class="element select medium" id="element_2" name="element_2">
                            <option value="off" <?php if($arr[0]["lamp"] == "off") echo('selected="selected"'); ?> >Выключить</option>
                            <option value="on" <?php if($arr[0]["lamp"] == "on") echo('selected="selected"'); ?> >Включить</option>
                        </select>
                    </div>
                </li>
                <li id="li_3">
                    <label class="description" for="element_3">Pump </label>
                    <div>
                        <select class="element select medium" id="element_3" name="element_3">
                            <option value="off" <?php if($arr[0]["pump"] == "off") echo('selected="selected"'); ?> >Выключить</option>
                            <option value="on" <?php if($arr[0]["pump"] == "on") echo('selected="selected"'); ?> >Включить</option>
                        </select>
                    </div>
                </li>
                <li id="li_4">
                    <label class="description" for="element_4">LED </label>
                    <div>
                        <input type="color" value="<?php echo($arr[0]["led"]); ?>" name="element_4">
                    </div>
                </li>

                <li class="buttons">
                    <input type="hidden" name="form_id" value="93544" />
                    <input id="saveForm" class="button_text" type="submit" name="submit" value="Применить" />
                </li>
            </ul>
        </form>
    </div>
    <img id="bottom" src="bottom.png" alt="">
    <?php
    include "../../../config/custom.php"; 
    if (isset($_POST['element_1']) && isset($_POST['element_2']) && isset($_POST['element_3']) && ($_POST['element_1'] == "off" || $_POST['element_1'] == "on") && ($_POST['element_2'] == "off" || $_POST['element_2'] == "on") && ($_POST['element_3'] == "off" || $_POST['element_3'] == "on")){
        $fan = $mysqli->real_escape_string(trim($_POST['element_1']));
        $lamp = $mysqli->real_escape_string(trim($_POST['element_2']));
        $pump = $mysqli->real_escape_string(trim($_POST['element_3']));
        $led = $mysqli->real_escape_string(trim($_POST['element_4']));
        if($mysqli->query("UPDATE `presets` SET `lamp`='$lamp',`pump`='$pump',`fan`='$fan',`led`='$led',`editdate`=NOW() WHERE presetid='$presetid'")){
            phpPost_redirect('../../', array('type' => 'success', 'title' => 'Обновление пресета', 'msg' => 'Пресет успешно обновлён'));
        }else{
            phpAlert_engine("error", "Системная ошибка", "Ошибка при обновлении пресета");
        } 
    }elseif(isset($_POST['element_4'])){
        phpAlert_engine('error', 'Ошибка', 'Проверьте вводимые данные');
    }
    ?>
</body>

</html>