<?php
    error_reporting(E_ALL & ~E_NOTICE);
    include "../../../config/userauth.php";
    $presetid = -1;
	if(isset($_GET['presetid'])){
		$presetid = intval($mysqli->real_escape_string(trim($_GET['presetid'])));
	}else{
		header('Location: ' . $site_host . 'panel');
		exit(0);
    }
    $auth_check = false;
    if($presetid != -1 && intval($mysqli->query("SELECT `userid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"userid"}) == $userid){
        $auth_check = true;
	}else{
		setcookie("UID", "", time() - 3600);
		$userid = -1;
		header('Location: ' . $site_host);
		exit(0);
    }
?>


<html>
    <head>
        <title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>"> 
    </head>
    <body>
        <h2><a href="../../">Вернуться</a></h2>
        <?php include "../../../config/custom.php"; 
        if(intval($mysqli->query("SELECT `farmid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"farmid"}) != -1){
            phpPost_redirect('../../', array('type' => 'error', 'title' => 'Удаление пресета', 'msg' => 'Вы не можете удалить ручной пресет'));
            $presetid = -1;
            exit(0);
        }elseif ($auth_check && intval($mysqli->query("SELECT `farmid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"farmid"}) == -1){
            if($auth_check && $mysqli->query("DELETE FROM `presets` WHERE presetid='$presetid';")){
                $arr = $mysqli->query("SELECT * FROM `farms` WHERE (userid='$userid' AND presetid='$presetid');")->fetch_all(MYSQLI_ASSOC);
                for($i = 0; $i < count($arr); $i++){
                    $farmid = $arr[$i]["farmid"];
                    $new_presetid = intval($mysqli->query("SELECT `presetid` FROM `presets` WHERE farmid='$farmid';")->fetch_object()->{"presetid"});
                    $mysqli->query("UPDATE `farms` SET `presetid`='$new_presetid' WHERE farmid='$farmid';");
                }
                phpPost_redirect('../../', array('type' => 'success', 'title' => 'Удаление пресета', 'msg' => 'Пресет успешно удален'));
            }else{
                phpPost_redirect('../../', array('type' => 'error', 'title' => 'Удаление пресета', 'msg' => 'Пресет не удален из-за системной ошибки'));
            }
        }   
        ?>
    </body>
</html>