<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <?php
    include "../../../config/userauth.php";
    $presetid = -1;
	if(isset($_GET['presetid'])){
		$presetid = intval($mysqli->real_escape_string(trim($_GET['presetid'])));
	}else{
		header('Location: ' . $site_host . 'panel');
		exit(0);
    }
	$auth_check = false;
	if($presetid != -1 && intval($mysqli->query("SELECT `userid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"userid"}) == $userid){
        if(intval($mysqli->query("SELECT `farmid` FROM `presets` WHERE presetid='$presetid';")->fetch_object()->{"farmid"}) != -1){
            header('Location: ' . $site_host . 'panel/presets/editmanual?presetid=' . $presetid);
            $presetid = -1;
            exit(0);
        }else{
            $auth_check = true;
        }   
	}else{
		setcookie("UID", "", time() - 3600);
		$userid = -1;
		header('Location: ' . $site_host);
		exit(0);
    }
    $arr = $mysqli->query("SELECT * FROM `presets` WHERE presetid='$presetid';")->fetch_all(MYSQLI_ASSOC);
    $old_presetname = $arr[0]["presetname"];
    $preset_fan = json_decode($arr[0]["fan"], true);
    $preset_pump = json_decode($arr[0]["pump"], true);
    $preset_lamp = json_decode($arr[0]["lamp"], true);
    ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Редактирование пресета</title>
    <link rel="stylesheet" type="text/css" href="view.css" media="all">
    <script type="text/javascript" src="view.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        body {
            font-family: Arial;
        }
        /* Style the tab */
        
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
        /* Style the buttons inside the tab */
        
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
            font-size: 17px;
        }
        /* Change background color of buttons on hover */
        
        .tab button:hover {
            background-color: #ddd;
        }
        /* Create an active/current tablink class */
        
        .tab button.active {
            background-color: #ccc;
        }
        /* Style the tab content */
        
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>
</head>

<body id="main_body">
    <img id="top" src="top.png" alt="">
    <div id="form_container">
        <h1><a>Редактирование пресета</a></h1>
        <div id="tabs" class="tabs">
            <div class="form_description">
                <h2>Редактирование пресета</h2>
                <p>Оставьте поля пустыми, если не нужно учитывать данный параметр<br><a href="../..">Вернуться</a></p>
            </div>
            <div class="tab">
                <button class="tablinks" onclick="openForm(event, 'fan')">Fan</button>
                <button class="tablinks" onclick="openForm(event, 'lamp')">Lamp</button>
                <button class="tablinks" onclick="openForm(event, 'pump')">Pump</button>
                <button class="tablinks" onclick="openForm(event, 'led')">LED</button>
            </div>
        </div>
        <form id="form_93510" class="appnitro" method="post" action="">
            <li class="section_break">
                <label class="description" for="name">Название пресета </label>
                <span>
							<input id="name" name="name" class="element text large" type="text" maxlength="255" value="<?php echo($old_presetname); ?>" title="Введите название пресета" required/>
							</span>
            </li>

            <div id="fan" class="tabcontent">
                <ul>
                    <h3>Параметры кулера</h3>
                    <li class="section_break">
                        <h3>Включение по времени:</h3>
                        <p></p>
                    </li>
                    <li id="li_2">
                        <label class="description" for="fan_element_2">Включить на </label>
                        <span>
								<input id="fan_element_2_1" name="fan_element_2_1" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_fan['on_time_h']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>HH</label>
							</span>
                        <span>
								<input id="fan_element_2_2" name="fan_element_2_2" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_fan['on_time_m']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>MM</label>
							</span>
                        <span>
								<input id="fan_element_2_3" name="fan_element_2_3" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_fan['on_time_s']); ?>" pattern="[0-9]+" title="Введите только числа" />
								<label>SS</label>
							</span>
                        <p class="guidelines" id="guide_2"><small>Введите время активности устройства</small></p>
                    </li>
                    <li id="li_3">
                        <label class="description" for="fan_element_3">С перерывом </label>
                        <span>
								<input id="fan_element_3_1" name="fan_element_3_1" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_fan['off_time_h']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>HH</label>
							</span>
                        <span>
								<input id="fan_element_3_2" name="fan_element_3_2" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_fan['off_time_m']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>MM</label>
							</span>
                        <span>
								<input id="fan_element_3_3" name="fan_element_3_3" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_fan['off_time_s']); ?>" pattern="[0-9]+" title="Введите только числа" />
								<label>SS</label>
							</span>
                        <p class="guidelines" id="guide_3"><small>Введите перерыв между включениями</small></p>
                    </li>
                    <li class="section_break">
                        <h3>Включение по температуре:</h3>
                        <p></p>
                    </li>
                    <li id="li_6">
                        <label class="description" for="fan_element_6">Температура срабатывания </label>
                        <div>
                            <input id="fan_element_6" name="fan_element_6" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_fan['temp']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_6"><small>Температура срабатывания пресета</small></p>
                    </li>
                    <li id="li_6_len">
                        <label class="description" for="fan_element_6_len">Длительность </label>
                        <div>
                            <input id="fan_element_6_len" name="fan_element_6_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_fan['temp_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_6_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_11">
                        <label class="description" for="fan_element_11">Действие </label>
                        <div>
                            <select class="element select medium" id="fan_element_11" name="fan_element_11">
                                <option value="1" <?php if(intval($preset_fan['temp_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_fan['temp_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_fan['temp_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>
                    <li class="section_break">
                        <h3>Включение по значению влажности:</h3>
                        <p></p>
                    </li>
                    <li id="li_8">
                        <label class="description" for="fan_element_8">Порог влажности </label>
                        <div>
                            <input id="fan_element_8" name="fan_element_8" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_fan['humidity']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_8"><small>Значение влажности срабатывания пресета</small></p>
                    </li>
                    <li id="li_8_len">
                        <label class="description" for="fan_element_8_len">Длительность </label>
                        <div>
                            <input id="fan_element_8_len" name="fan_element_8_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_fan['humidity_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_8_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_12">
                        <label class="description" for="fan_element_12">Действие </label>
                        <div>
                            <select class="element select medium" id="fan_element_12" name="fan_element_12">
                                <option value="1" <?php if(intval($preset_fan['humidity_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_fan['humidity_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_fan['humidity_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>
                    <li class="section_break">
                        <h3>Включение по значению света:</h3>
                        <p></p>
                    </li>
                    <li id="li_10">
                        <label class="description" for="fan_element_10">Порог света </label>
                        <div>
                            <input id="fan_element_10" name="fan_element_10" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_fan['light']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_10"><small>Значение света срабатывания пресета</small></p>
                    </li>
                    <li id="li_10_len">
                        <label class="description" for="fan_element_10_len">Длительность </label>
                        <div>
                            <input id="fan_element_10_len" name="fan_element_10_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_fan['light_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_10_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_13">
                        <label class="description" for="fan_element_13">Действие </label>
                        <div>
                            <select class="element select medium" id="fan_element_13" name="fan_element_13">
                                <option value="1" <?php if(intval($preset_fan['light_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_fan['light_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_fan['light_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>

                    <li class="buttons">
                        <input type="hidden" name="form_id" value="93510" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Применить" />
                    </li>
                </ul>
            </div>

            <div id="lamp" class="tabcontent">
                <ul>
                    <h3>Параметры лампы</h3>
                    <li class="section_break">
                        <h3>Включение по времени:</h3>
                        <p></p>
                    </li>
                    <li id="li_2">
                        <label class="description" for="lamp_element_2">Включить на </label>
                        <span>
								<input id="lamp_element_2_1" name="lamp_element_2_1" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_lamp['on_time_h']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>HH</label>
							</span>
                        <span>
								<input id="lamp_element_2_2" name="lamp_element_2_2" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_lamp['on_time_m']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>MM</label>
							</span>
                        <span>
								<input id="lamp_element_2_3" name="lamp_element_2_3" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_lamp['on_time_s']); ?>" pattern="[0-9]+" title="Введите только числа" />
								<label>SS</label>
							</span>
                        <p class="guidelines" id="guide_2"><small>Введите время активности устройства</small></p>
                    </li>
                    <li id="li_3">
                        <label class="description" for="lamp_element_3">С перерывом </label>
                        <span>
								<input id="lamp_element_3_1" name="lamp_element_3_1" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_lamp['off_time_h']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>HH</label>
							</span>
                        <span>
								<input id="lamp_element_3_2" name="lamp_element_3_2" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_lamp['off_time_m']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>MM</label>
							</span>
                        <span>
								<input id="lamp_element_3_3" name="lamp_element_3_3" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_lamp['off_time_s']); ?>" pattern="[0-9]+" title="Введите только числа" />
								<label>SS</label>
							</span>
                        <p class="guidelines" id="guide_3"><small>Введите перерыв между включениями</small></p>
                    </li>
                    <li class="section_break">
                        <h3>Включение по температуре:</h3>
                        <p></p>
                    </li>
                    <li id="li_6">
                        <label class="description" for="lamp_element_6">Температура срабатывания </label>
                        <div>
                            <input id="lamp_element_6" name="lamp_element_6" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_lamp['temp']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_6"><small>Температура срабатывания пресета</small></p>
                    </li>
                    <li id="li_6_len">
                        <label class="description" for="lamp_element_6_len">Длительность </label>
                        <div>
                            <input id="lamp_element_6_len" name="lamp_element_6_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_lamp['temp_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_6_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_11">
                        <label class="description" for="lamp_element_11">Действие </label>
                        <div>
                            <select class="element select medium" id="lamp_element_11" name="lamp_element_11">
                                <option value="1" <?php if(intval($preset_lamp['temp_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_lamp['temp_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_lamp['temp_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>
                    <li class="section_break">
                        <h3>Включение по значению влажности:</h3>
                        <p></p>
                    </li>
                    <li id="li_8">
                        <label class="description" for="lamp_element_8">Порог влажности </label>
                        <div>
                            <input id="lamp_element_8" name="lamp_element_8" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_lamp['humidity']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_8"><small>Значение влажности срабатывания пресета</small></p>
                    </li>
                    <li id="li_8_len">
                        <label class="description" for="lamp_element_8_len">Длительность </label>
                        <div>
                            <input id="lamp_element_8_len" name="lamp_element_8_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_lamp['humidity_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_8_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_12">
                        <label class="description" for="lamp_element_12">Действие </label>
                        <div>
                            <select class="element select medium" id="lamp_element_12" name="lamp_element_12">
                                <option value="1" <?php if(intval($preset_lamp['humidity_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_lamp['humidity_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_lamp['humidity_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>
                    <li class="section_break">
                        <h3>Включение по значению света:</h3>
                        <p></p>
                    </li>
                    <li id="li_10">
                        <label class="description" for="lamp_element_10">Порог света </label>
                        <div>
                            <input id="lamp_element_10" name="lamp_element_10" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_lamp['light']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_10"><small>Значение света срабатывания пресета</small></p>
                    </li>
                    <li id="li_10_len">
                        <label class="description" for="lamp_element_10_len">Длительность </label>
                        <div>
                            <input id="lamp_element_10_len" name="lamp_element_10_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_lamp['light_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_10_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_13">
                        <label class="description" for="lamp_element_13">Действие </label>
                        <div>
                            <select class="element select medium" id="lamp_element_13" name="lamp_element_13">
                                <option value="1" <?php if(intval($preset_lamp['light_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_lamp['light_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_lamp['light_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>

                    <li class="buttons">
                        <input type="hidden" name="form_id" value="93510" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Применить" />
                    </li>
                </ul>
            </div>

            <div id="pump" class="tabcontent">
                <ul>
                    <h3>Параметры помпы</h3>
                    <li class="section_break">
                        <h3>Включение по времени:</h3>
                        <p></p>
                    </li>
                    <li id="li_2">
                        <label class="description" for="pump_element_2">Включить на </label>
                        <span>
								<input id="pump_element_2_1" name="pump_element_2_1" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_pump['on_time_h']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>HH</label>
							</span>
                        <span>
								<input id="pump_element_2_2" name="pump_element_2_2" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_pump['on_time_m']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>MM</label>
							</span>
                        <span>
								<input id="pump_element_2_3" name="pump_element_2_3" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_pump['on_time_s']); ?>" pattern="[0-9]+" title="Введите только числа" />
								<label>SS</label>
							</span>
                        <p class="guidelines" id="guide_2"><small>Введите время активности устройства</small></p>
                    </li>
                    <li id="li_3">
                        <label class="description" for="pump_element_3">С перерывом </label>
                        <span>
								<input id="pump_element_3_1" name="pump_element_3_1" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_pump['off_time_h']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>HH</label>
							</span>
                        <span>
								<input id="pump_element_3_2" name="pump_element_3_2" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_pump['off_time_m']); ?>" pattern="[0-9]+" title="Введите только числа" /> : 
								<label>MM</label>
							</span>
                        <span>
								<input id="pump_element_3_3" name="pump_element_3_3" class="element text " size="2" type="text" maxlength="2" value="<?php echo($preset_pump['off_time_s']); ?>" pattern="[0-9]+" title="Введите только числа" />
								<label>SS</label>
							</span>
                        <p class="guidelines" id="guide_3"><small>Введите перерыв между включениями</small></p>
                    </li>
                    <li class="section_break">
                        <h3>Включение по температуре:</h3>
                        <p></p>
                    </li>
                    <li id="li_6">
                        <label class="description" for="pump_element_6">Температура срабатывания </label>
                        <div>
                            <input id="pump_element_6" name="pump_element_6" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_pump['temp']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_6"><small>Температура срабатывания пресета</small></p>
                    </li>
                    <li id="li_6_len">
                        <label class="description" for="pump_element_6_len">Длительность </label>
                        <div>
                            <input id="pump_element_6_len" name="pump_element_6_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_pump['temp_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_6_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_11">
                        <label class="description" for="pump_element_11">Действие </label>
                        <div>
                            <select class="element select medium" id="pump_element_11" name="pump_element_11">
                                <option value="1" <?php if(intval($preset_pump['temp_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_pump['temp_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_pump['temp_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>
                    <li class="section_break">
                        <h3>Включение по значению влажности:</h3>
                        <p></p>
                    </li>
                    <li id="li_8">
                        <label class="description" for="pump_element_8">Порог влажности </label>
                        <div>
                            <input id="pump_element_8" name="pump_element_8" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_pump['humidity']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_8"><small>Значение влажности срабатывания пресета</small></p>
                    </li>
                    <li id="li_8_len">
                        <label class="description" for="pump_element_8_len">Длительность </label>
                        <div>
                            <input id="pump_element_8_len" name="pump_element_8_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_pump['humidity_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_8_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_12">
                        <label class="description" for="pump_element_12">Действие </label>
                        <div>
                            <select class="element select medium" id="pump_element_12" name="pump_element_12">
                                <option value="1" <?php if(intval($preset_pump['humidity_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_pump['humidity_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_pump['humidity_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>
                    <li class="section_break">
                        <h3>Включение по значению света:</h3>
                        <p></p>
                    </li>
                    <li id="li_10">
                        <label class="description" for="pump_element_10">Порог света </label>
                        <div>
                            <input id="pump_element_10" name="pump_element_10" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_pump['light']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_10"><small>Значение света срабатывания пресета</small></p>
                    </li>
                    <li id="li_10_len">
                        <label class="description" for="pump_element_10_len">Длительность </label>
                        <div>
                            <input id="pump_element_10_len" name="pump_element_10_len" class="element text medium" type="text" maxlength="255" value="<?php echo($preset_pump['light_len']); ?>" pattern="[0-9]+" title="Введите только числа" />
                        </div>
                        <p class="guidelines" id="guide_10_len"><small>Длительность включения</small></p>
                    </li>
                    <li id="li_13">
                        <label class="description" for="pump_element_13">Действие </label>
                        <div>
                            <select class="element select medium" id="pump_element_13" name="pump_element_13">
                                <option value="1" <?php if(intval($preset_pump['light_act']) == 1) echo('selected="selected"'); ?> >Игнорировать</option>
                                <option value="2" <?php if(intval($preset_pump['light_act']) == 2) echo('selected="selected"'); ?> >Включить</option>
                                <option value="3" <?php if(intval($preset_pump['light_act']) == 3) echo('selected="selected"'); ?> >Выключить</option>
                            </select>
                        </div>
                    </li>

                    <li class="buttons">
                        <input type="hidden" name="form_id" value="93510" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Применить" />
                    </li>
                </ul>
            </div>

            <div id="led" class="tabcontent">
                <ul>
                    <h3>Параметры LED</h3>
                    <li class="section_break">
                        <h3></h3>
                        <p></p>
                    </li>
                    <li id="li_2">
                        <label class="description" for="pump_element_2">Выберите цвет </label>
                        <span>
							<input type="color" value="<?php echo($arr[0]["led"]); ?>" name="led_color">
							</span>
                    </li>

                    <li class="buttons">
                        <input type="hidden" name="form_id" value="93510" />
                        <input id="saveForm" class="button_text" type="submit" name="submit" value="Применить" />
                    </li>
                </ul>
            </div>
        </form>
    </div>
    <img id="bottom" src="bottom.png" alt="">
    <?php
    include "../../../config/custom.php"; 
    $inputs = array(
        'name',
        'fan_element_2_1',
        'fan_element_2_2',
        'fan_element_2_3',
        'fan_element_3_1',
        'fan_element_3_2',
        'fan_element_3_3',
        'fan_element_6',
        'fan_element_11',
        'fan_element_8',
        'fan_element_12',
        'fan_element_10',
        'fan_element_13',
        'lamp_element_2_1',
        'lamp_element_2_2',
        'lamp_element_2_3',
        'lamp_element_3_1',
        'lamp_element_3_2',
        'lamp_element_3_3',
        'lamp_element_6',
        'lamp_element_11',
        'lamp_element_8',
        'lamp_element_12',
        'lamp_element_10',
        'lamp_element_13',
        'pump_element_2_1',
        'pump_element_2_2',
        'pump_element_2_3',
        'pump_element_3_1',
        'pump_element_3_2',
        'pump_element_3_3',
        'pump_element_6',
        'pump_element_11',
        'pump_element_8',
        'pump_element_12',
        'pump_element_10',
        'pump_element_13',
        'led_color',
        'fan_element_6_len',
        'fan_element_8_len',
        'fan_element_10_len',
        'lamp_element_6_len',
        'lamp_element_8_len',
        'lamp_element_10_len',
        'pump_element_6_len',
        'pump_element_8_len',
        'pump_element_10_len'
    );
    if(isset($_POST['name']) && $auth_check){
        foreach ($inputs as &$input)
            if(is_numeric($_POST[$input]))
                $input = $mysqli->real_escape_string(trim($_POST[$input]));
            else
                $input = '';
        $preset_name = $mysqli->real_escape_string(trim($_POST['name']));
        if($mysqli->query("SELECT COUNT(*) FROM `presets` WHERE (presetname='$preset_name' AND userid='$userid');")->fetch_object()->{"COUNT(*)"} == "0" || $preset_name == $old_presetname){
            if( ($inputs[8] > 1 && $inputs[7] == '' && $inputs[38] == '') || ($inputs[10] > 1 && $inputs[9] == '' && $inputs[39] == '') || ($inputs[12] > 1 && $inputs[11] == '' && $inputs[40] == '') || ($inputs[20] > 1 && $inputs[19] == '' && $inputs[41] == '') || ($inputs[22] > 1 && $inputs[21] == '' && $inputs[42] == '') || ($inputs[24] > 1 && $inputs[23] == '' && $inputs[43] == '') || ($inputs[32] > 1 && $inputs[31] == '' && $inputs[44] == '') || ($inputs[34] > 1 && $inputs[33] == '' && $inputs[45] == '') || ($inputs[36] > 1 && $inputs[35] == '' && $inputs[46] == '') ){
                phpAlert_engine('error', 'Ошибка', 'Проверьте вводимые данные');
            }else{
                $preset_fan = array(
                    'on_time_h' => $inputs[1],
                    'on_time_m' => $inputs[2],
                    'on_time_s' => $inputs[3],
                    'off_time_h' => $inputs[4],
                    'off_time_m' => $inputs[5],
                    'off_time_s' => $inputs[6],
                    'last_on_time_h' => 0,
                    'last_on_time_m' => 0,
                    'last_on_time_s' => 0,
                    'temp' => $inputs[7],
                    'temp_len' => $inputs[38],
                    'temp_act' => $inputs[8],
                    'humidity' => $inputs[9],
                    'humidity_len' => $inputs[39],
                    'humidity_act' => $inputs[10],
                    'light' => $inputs[11],
                    'light_len' => $inputs[40],
                    'light_act' => $inputs[12]
                );
                $preset_lamp = array(
                    'on_time_h' => $inputs[13],
                    'on_time_m' => $inputs[14],
                    'on_time_s' => $inputs[15],
                    'off_time_h' => $inputs[16],
                    'off_time_m' => $inputs[17],
                    'off_time_s' => $inputs[18],
                    'last_on_time_h' => 0,
                    'last_on_time_m' => 0,
                    'last_on_time_s' => 0,
                    'temp' => $inputs[19],
                    'temp_len' => $inputs[41],
                    'temp_act' => $inputs[20],
                    'humidity' => $inputs[21],
                    'humidity_len' => $inputs[42],
                    'humidity_act' => $inputs[22],
                    'light' => $inputs[23],
                    'light_len' => $inputs[43],
                    'light_act' => $inputs[24]
                );
                $preset_pump = array(
                    'on_time_h' => $inputs[25],
                    'on_time_m' => $inputs[26],
                    'on_time_s' => $inputs[27],
                    'off_time_h' => $inputs[28],
                    'off_time_m' => $inputs[29],
                    'off_time_s' => $inputs[30],
                    'last_on_time_h' => 0,
                    'last_on_time_m' => 0,
                    'last_on_time_s' => 0,
                    'temp' => $inputs[31],
                    'temp_len' => $inputs[44],
                    'temp_act' => $inputs[32],
                    'humidity' => $inputs[33],
                    'humidity_len' => $inputs[45],
                    'humidity_act' => $inputs[34],
                    'light' => $inputs[35],
                    'light_len' => $inputs[46],
                    'light_act' => $inputs[36]
                );
                $preset_fan_json = json_encode($preset_fan);
                $preset_lamp_json = json_encode($preset_lamp);
                $preset_pump_json = json_encode($preset_pump);
                $preset_led = $mysqli->real_escape_string(trim($_POST['led_color']));

                if($mysqli->query("UPDATE `presets` SET `presetname`='$preset_name',`lamp`='$preset_lamp_json',`pump`='$preset_pump_json',`fan`='$preset_fan_json',`led`='$preset_led',`editdate`=NOW() WHERE presetid='$presetid'")){
                    phpPost_redirect('../../', array('type' => 'success', 'title' => 'Обновление пресета', 'msg' => 'Пресет успешно обновлён'));
                }else{
                    phpAlert_engine("error", "Системная ошибка", "Ошибка при обновлении пресета");
                } 
            }
        }else{
            phpAlert_engine('error', 'Ошибка', 'Пресет с таким названием уже существует');
        }
    }
    
    ?>
    <script>
        function openForm(evt, formName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(formName).style.display = "block";
            evt.currentTarget.className += " active";
        }
		openForm(event, 'fan')
    </script>
</body>

</html>