<?php
    error_reporting(E_ALL & ~E_NOTICE);
    include "../../../config/userauth.php";
    $farmid = -1;
    if(isset($_GET['farmid'])){
        $farmid = intval($mysqli->real_escape_string(trim($_GET['farmid'])));
    }else{
        header('Location: ' . $site_host . 'panel');
        exit(0);
    }
    $auth_check = false;
    if($farmid != -1 && intval($mysqli->query("SELECT `userid` FROM `farms` WHERE farmid='$farmid';")->fetch_object()->{"userid"}) == $userid){
        $auth_check = true;
    }else{
        setcookie("UID", "", time() - 3600);
        $userid = -1;
        header('Location: ' . $site_host);
        exit(0);
    }
?>


<html>
    <head>
        <title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>"> 
    </head>
    <body>
        <h2><a href="../../">Вернуться</a></h2>
        <?php include "../../../config/custom.php"; 
            if($auth_check && $mysqli->query("DELETE FROM `farms` WHERE farmid='$farmid';") && $mysqli->query("DELETE FROM `presets` WHERE farmid='$farmid';")){
                phpPost_redirect('../../', array('type' => 'success', 'title' => 'Удаление фермы', 'msg' => 'Ферма успешно удалена'));
            }else{
                phpPost_redirect('../../', array('type' => 'error', 'title' => 'Удаление фермы', 'msg' => 'Ферма не удалена из-за системной ошибки'));
            }
        ?>
    </body>
</html>