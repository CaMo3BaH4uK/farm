<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include "../../../config/userauth.php";
	$farmid = -1;
	if(isset($_GET['farmid'])){
		$farmid = intval($mysqli->real_escape_string(trim($_GET['farmid'])));
	}else{
		header('Location: ' . $site_host . 'panel');
		exit(0);
	}
	$auth_check = false;
	if($farmid != -1 && intval($mysqli->query("SELECT `userid` FROM `farms` WHERE farmid='$farmid';")->fetch_object()->{"userid"}) == $userid){
		$auth_check = true;
	}else{
		setcookie("UID", "", time() - 3600);
		$userid = -1;
		header('Location: ' . $site_host);
		exit(0);
	}
	$farmname = $mysqli->query("SELECT `farmname` FROM `farms` WHERE farmid='$farmid';")->fetch_object()->{"farmname"};
	$current_preset = intval($mysqli->query("SELECT `presetid` FROM `farms` WHERE farmid='$farmid';")->fetch_object()->{"presetid"});
	$current_preset_name = $mysqli->query("SELECT `presetname` FROM `presets` WHERE presetid='$current_preset';")->fetch_object()->{"presetname"};
	$default_preset = intval($mysqli->query("SELECT `presetid` FROM `presets` WHERE farmid='$farmid';")->fetch_object()->{"presetid"});
	$default_preset_name = $mysqli->query("SELECT `presetname` FROM `presets` WHERE presetid='$default_preset';")->fetch_object()->{"presetname"};
	$arr = $mysqli->query("SELECT * FROM `presets` WHERE userid='$userid';")->fetch_all(MYSQLI_ASSOC);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>">
		<link rel="stylesheet" type="text/css" href="view.css" media="all">
		<script type="text/javascript" src="view.js"></script>
	</head>
	<body id="main_body" >
	<img id="top" src="top.png" alt="">
	<div id="form_container">
		<h1><a>Изменение параметров фермы</a></h1>
		<form id="form_93091" class="appnitro"  method="post" action="#">
			<div class="form_description">
				<h2>Изменение параметров фермы</h2>
				<p>Введите новое название или выберите пресет<br><a href="../..">Вернуться</a></p>
			</div>						
			<ul>
				<li id="li_1">
					<label class="description" for="edit_farm_name">Название фермы </label>
					<div>
						<input required title="Введите название фермы" id="edit_farm_name" name="edit_farm_name" class="element text medium" type="text" maxlength="255" value="<?php echo $farmname; ?>"/> 
					</div> 
				</li>
				<li id="li_3" >
					<label class="description" for="new_preset_id">Текущий пресет</label>
					<div>
					<select class="element select medium" id="new_preset_id" name="new_preset_id" required> 
						<?php
							echo '<option selected value="' . $current_preset . '">' . $current_preset_name . '</option>';
							if($current_preset != $default_preset)
								echo '<option value="' . $default_preset . '">' . $default_preset_name . '</option>';
							for($i = 0; $i < count($arr); $i++)
								if(intval($arr[$i]['presetid']) != $current_preset && intval($arr[$i]['presetid']) != $default_preset)
									echo '<option value="' . $arr[$i]["presetid"] . '">' . $arr[$i]["presetname"] . '</option>';
						?>
					</select>
					</div> 
				</li>
				<li class="buttons">
			    	<input type="hidden" name="form_id" value="93091"/>
					<input id="saveForm" class="button_text" type="submit" name="submit" value="Применить" />
				</li>
			</ul>
		</form>	
	</div>
	<img id="bottom" src="bottom.png" alt="">
	<?php include "../../../config/custom.php"; 
		if ($auth_check){
			if(isset($_POST['edit_farm_name']) && isset($_POST['new_preset_id']) && trim($_POST['edit_farm_name']) != NULL && trim($_POST['new_preset_id']) != NULL){
				$new_farmname = $mysqli->real_escape_string(trim($_POST['edit_farm_name']));
				$new_presetid = intval($mysqli->real_escape_string(trim($_POST['new_preset_id'])));
				$farmname = $mysqli->query("SELECT `farmname` FROM `farms` WHERE farmid='$farmid';")->fetch_object()->{"farmname"};
				if( ($mysqli->query("SELECT COUNT(*) FROM `farms` WHERE (farmname='$new_farmname' AND userid='$userid');")->fetch_object()->{"COUNT(*)"} == "0" || $new_farmname == $farmname) && intval($mysqli->query("SELECT `userid` FROM `presets` WHERE presetid='$new_presetid';")->fetch_object()->{"userid"}) == $userid){
					$mysqli->query("UPDATE `farms` SET `farmname`='$new_farmname',`presetid`='$new_presetid' WHERE farmid='$farmid';");
					$mysqli->query("UPDATE `presets` SET `presetname`='Ручной режим для фермы $new_farmname (ID фермы: $farmid)' WHERE farmid='$farmid';");
					phpPost_redirect('../../', array('type' => 'success', 'title' => 'Редактирование фермы', 'msg' => 'Ферма успешно отредактирована'));
				}else{
					phpAlert_engine("warning", "Ошибка", "Ферма с таким названием уже существует");
				}
			}
		}
	?>
	</body>
</html>