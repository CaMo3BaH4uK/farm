<?php
    include "../../../config/userauth.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Регистрация новой фермы</title>
		<link rel="stylesheet" type="text/css" href="view.css" media="all">
		<script type="text/javascript" src="view.js"></script>
	</head>
	<body id="main_body" >
	<img id="top" src="top.png" alt="">
	<div id="form_container">
		<h1><a>Регистрация новой фермы</a></h1>
		<form id="form_93091" class="appnitro"  method="post" action="#">
			<div class="form_description">
				<h2>Регистрация новой фермы</h2>
				<p>Введите название фермы для регистрации её в вашем аккаунте<br><a href="../..">Вернуться</a></p>
			</div>						
			<ul>
				<li id="li_1">
					<label class="description" for="register_farm_name">Название фермы </label>
					<div>
						<input required id="register_farm_name" name="register_farm_name" class="element text medium" type="text" maxlength="255" value=""/> 
					</div> 
				</li>
				<li class="buttons">
			    	<input type="hidden" name="form_id" value="93091"/>
					<input id="saveForm" class="button_text" type="submit" name="submit" value="Зарегистрировать" />
				</li>
			</ul>
		</form>	
	</div>
	<img id="bottom" src="bottom.png" alt="">
	<?php include "../../../config/custom.php"; 
	    if(isset($_POST["register_farm_name"]) && trim($_POST["register_farm_name"]) != NULL ) { 
			$farmname = $mysqli->real_escape_string(trim($_POST["register_farm_name"])); 
			if($mysqli->query("SELECT COUNT(*) FROM `farms` WHERE (farmname='$farmname' AND userid='$userid');")->fetch_object()->{"COUNT(*)"} == "0"){
				$request = $mysqli->query("SELECT MAX(farmid) FROM farms")->fetch_object()->{"MAX(farmid)"};
				if($request == NULL)
					$farmid = 1;
				else
					$farmid = intval($request) + 1;
	
				$request = $mysqli->query("SELECT MAX(presetid) FROM presets")->fetch_object()->{"MAX(presetid)"};
				if($request == NULL)
					$presetid = 1;
				else
					$presetid = intval($request) + 1;
	
				$farmtoken = "";
				while(true){
					$farmtoken = bin2hex(random_bytes(5));
					if($mysqli->query("SELECT COUNT(*) FROM `farms` WHERE farmtoken='$farmtoken';")->fetch_object()->{"COUNT(*)"} == "0")
						break;
				}
				if($mysqli->query("INSERT INTO `farms` (`farmid`, `farmname`, `userid`, `farmtoken`, `lastonline`, `presetid`, `temperature`, `humidity`, `light`, `tank`) VALUES ('$farmid', '$farmname', '$userid', '$farmtoken', NOW(), '$presetid', '0', '0', '0', '0');") && $mysqli->query("INSERT INTO `presets` (`userid`, `farmid`, `presetid`, `presetname`, `lamp`, `pump`, `fan`, `led`, `editdate`) VALUES ('$userid', '$farmid', '$presetid', 'Ручной режим для фермы $farmname (ID фермы: $farmid)', 'off', 'off', 'off', '#000000', NOW());")){
					phpPost_redirect('../../', array('type' => 'success', 'title' => 'Создание фермы', 'msg' => 'Ферма успешно создана'));
				}else{
					phpAlert_engine("error", "Системная ошибка", "Ошибка при создании фермы");
				} 
			}else{
				phpAlert_engine("warning", "Ошибка", "Ферма с таким названием уже существует");
			}
		}
	?>
	</body>
</html>