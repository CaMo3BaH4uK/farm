<?php
    include "../../config/config.php";
    $mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
	    if($mysqli->connect_errno){
            echo $mysql_error_msg;
            exit(0);
	}
    $farmtoken = '';
    if(isset($_GET['farmtoken'])){
        $farmtoken = $mysqli->real_escape_string(trim($_GET['farmtoken']));
        if($mysqli->query("SELECT COUNT(*) FROM `farms` WHERE farmtoken='$farmtoken';")->fetch_object()->{"COUNT(*)"} == "1"){
            if(isset($_GET['temperature']) && isset($_GET['humidity']) && isset($_GET['light']) && isset($_GET['tank'])){
                if(is_numeric(intval($_GET['temperature'])) && is_numeric(intval($_GET['humidity'])) && is_numeric(intval($_GET['light'])) && is_numeric(intval($_GET['tank']))){
                    $temperature = $mysqli->real_escape_string(trim($_GET['temperature']));
                    $humidity = $mysqli->real_escape_string(trim($_GET['humidity']));
                    $light = $mysqli->real_escape_string(trim($_GET['light']));
                    $tank = $mysqli->real_escape_string(trim($_GET['tank']));
                    if($mysqli->query("UPDATE `farms` SET `lastonline`=NOW(),`temperature`='$temperature',`humidity`='$humidity',`light`='$light',`tank`='$tank' WHERE farmtoken='$farmtoken'")){
                        echo "true";
                    }else{
                        echo "false";
                    } 
                }else{
                    echo "bad_data";
                }
            }else{
                echo "no_data";
            }
        }else{
            echo "bad_farmtoken";
        }
	}else{
		echo "no_farmtoken";
		exit(0);
    }