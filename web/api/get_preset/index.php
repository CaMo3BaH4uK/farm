<?php
    include "../../config/config.php";
    $mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
	    if($mysqli->connect_errno){
            echo $mysql_error_msg;
            exit(0);
	}
    $farmtoken = '';
	if(isset($_GET['farmtoken'])){
        $farmtoken = $mysqli->real_escape_string(trim($_GET['farmtoken']));
        if($mysqli->query("SELECT COUNT(*) FROM `farms` WHERE farmtoken='$farmtoken';")->fetch_object()->{"COUNT(*)"} == "1"){
            $presetid = intval($mysqli->query("SELECT `presetid` FROM `farms` WHERE farmtoken='$farmtoken';")->fetch_object()->{"presetid"});
            $arr = $mysqli->query("SELECT * FROM `presets` WHERE presetid='$presetid';")->fetch_all(MYSQLI_ASSOC);

            if(intval($arr[0]["farmid"]) == -1){
                echo $arr[0]["presetname"];
                echo "<br>";
                echo str_replace('""', '"0"', $arr[0]["fan"]);
                echo "<br>";
                echo str_replace('""', '"0"', $arr[0]["lamp"]);
                echo "<br>";
                echo str_replace('""', '"0"', $arr[0]["pump"]);
                echo "<br>";
                echo hexdec(str_replace('#', '', $arr[0]["led"]));
                echo "<br>";
                echo $arr[0]["editdate"];
                echo "<br>";
            }else{
                echo "manual";
                echo "<br>";
                echo str_replace('""', '"0"', $arr[0]["fan"]);
                echo "<br>";
                echo str_replace('""', '"0"', $arr[0]["lamp"]);
                echo "<br>";
                echo str_replace('""', '"0"', $arr[0]["pump"]);
                echo "<br>";
                echo hexdec(str_replace('#', '', $arr[0]["led"]));
                echo "<br>";
                echo $arr[0]["editdate"];
                echo "<br>";
            }
            
        }else{
            echo "bad_farmtoken";
        }
	}else{
		echo "no_farmtoken";
		exit(0);
    }