<?php
    include "config.php";
    $mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
    if($mysqli->connect_errno){
        echo $mysql_error_msg;
        exit(0);
    }
    $userid = -1;
    $auth = false;
    include "bans.php";
    if(isset($_COOKIE["UID"]))
        $userid = intval($mysqli->real_escape_string(trim($_COOKIE["UID"])));
    if($userid != -1 && $mysqli->query("SELECT `authtoken` FROM `users` WHERE userid='$userid';")->fetch_object()->{"authtoken"} == sha1($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'])){
        $email = $mysqli->query("SELECT `email` FROM `users` WHERE userid='$userid';")->fetch_object()->{"email"};
        $auth = true;
    }else{
        setcookie("UID", "", time() - 3600);
        $mysqli->close();
        header('Location: ' . $site_host . 'login');
        exit(0);
    }
    // $mysqli->close();
?>