<?php
echo '<link href="https://codeseven.github.io/toastr/build/toastr.min.css" rel="stylesheet" type="text/css">';
echo '<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>';
echo '<script src="https://codeseven.github.io/toastr/build/toastr.min.js"></script>';
// TYPES: success info warning error
$types = array(
    'success',
    'info',
    'warning',
    'error'
);


function phpAlert_engine($type, $title, $msg) {
    echo '<script type="text/javascript">toastr["' . $type . '"]("' . $msg . '", "' . $title . '")</script>';
}
function phpAlert_JS($msg) {
    echo '<script type="text/javascript">alert("' . $msg . '")</script>';
}
function phpPost_redirect($url, $data) {
    echo '<form id="fastpost" action="' . $url . '" method="post">';
    foreach ($data as $param => $cont) {
        echo '<input type="hidden" name="'.htmlentities($param).'" value="'.htmlentities($cont).'">';
    }
    echo '</form>';
    echo '<script type="text/javascript">document.getElementById("fastpost").submit();</script>';
}


if(isset($_POST["type"]) && isset($_POST["title"]) && isset($_POST["msg"]) && trim($_POST["type"]) != NULL && trim($_POST["title"]) != NULL && trim($_POST["msg"]) != NULL && in_array(trim($_POST["type"]), $types)){
    phpAlert_engine(trim($_POST["type"]), trim($_POST["title"]), trim($_POST["msg"]));
}