<?php
	error_reporting(E_ALL & ~E_NOTICE);
	include "../config/config.php";
	include "../config/bans.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>">
		<link rel="stylesheet" type="text/css" href="view.css" media="all">
		<script type="text/javascript" src="view.js"></script>
	</head>
	<body id="main_body" >
		<img id="top" src="top.png" alt="">
		<div id="form_container">
			<h1><a>Авторизация</a></h1>
			<form id="form_93095" class="appnitro"  method="post" action="#">
				<div class="form_description">
					<h2>Вход в панель управления</h2>
					<p>Не зарегистрированы? <a href="../register">Регистрация</a></p>
				</div>						
				<ul>
					<li id="li_1" >
						<label class="description" for="login_form_email">E-Mail </label>
						<div>
							<input required id="login_form_email" name="login_form_email" class="element text medium" type="text" maxlength="255" value="<?php if(isset($_POST['login_form_email'])) echo($_POST['login_form_email']); ?>" title="Введите верный E-mail" pattern="^(([-\w\d]+)(\.[-\w\d]+)*@([-\w\d]+)(\.[-\w\d]+)*(\.([a-zA-Z]{2,5}|[\d]{1,3})){1,2})$"/> 
						</div><p class="guidelines" id="guide_1"><small>Введите E-Mail</small></p> 
					</li>		
					<li id="li_2" >
						<label class="description" for="login_form_password">Пароль </label>
						<div>
							<input required id="login_form_password" name="login_form_password" class="element text medium" type="password" maxlength="255" value="<?php if(isset($_POST['register_form_password'])) echo($_POST['register_form_password']); ?>" title="Введите пароль" pattern="[\S]{8,}"><button id="login_form_password_show" name="login_form_password_show" type="button">Показать пароль</button></input> 
						</div><p class="guidelines" id="guide_2"><small>Введите пароль</small></p> 
					</li>
					<li class="buttons">
						<input type="hidden" name="form_id" value="93095" />
						<input id="saveForm" class="button_text" type="submit" name="submit" value="Войти" />
					</li>
				</ul>
			</form>	
		</div>
		<img id="bottom" src="bottom.png" alt="">
		<?php
			include "../config/custom.php";
			$mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
			if($mysqli->connect_errno){
				echo $mysql_error_msg;
				exit(0);
			}
			$checker = 0;
			if(isset($_POST["login_form_email"]) && $_POST["login_form_email"] != NULL) { $checker++; $email = $mysqli->real_escape_string(trim($_POST["login_form_email"])); }
			if(isset($_POST["login_form_password"]) && $_POST["login_form_password"] != NULL) { $checker++; $password = $mysqli->real_escape_string(trim($_POST["login_form_password"])); }
			if($checker == 2){
				if($mysqli->query("SELECT COUNT(*) FROM `users` WHERE email='$email';")->fetch_object()->{"COUNT(*)"}){
					$hashed_password = $mysqli->query("SELECT `password` FROM `users` WHERE email='$email';")->fetch_object()->{"password"};
					if (hash_equals($hashed_password, crypt($password, $hashed_password))){
						$userid = $mysqli->query("SELECT `userid` FROM `users` WHERE email='$email';")->fetch_object()->{"userid"};
						setcookie("UID", $userid, time()+60*60*24*30, "/");
						$authtoken = sha1($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR']);
						$mysqli->query("UPDATE `users` SET `authtoken`='$authtoken' WHERE userid='$userid';");
						phpPost_redirect('../panel/', array('type' => 'success', 'title' => 'Рады видеть вас снова', 'msg' => 'Вы успешно вошли в систему'));
					}else{
						phpAlert_engine('error', 'Ошибка', 'Ошибка при вводе E-mail или пароля!');
					}
				}else{
					phpAlert_engine('error', 'Ошибка', 'Ошибка при вводе E-mail или пароля!');
				}
			}
			$mysqli->close();
		?>
		<script>
			$("#login_form_password_show").hover(function() {
				$("#login_form_password").attr("type", "text");
			}, function () {
				$("#login_form_password").attr("type", "password");
			});
		</script>
	</body>
</html>