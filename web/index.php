<?php
    include "config/config.php";
    $mysqli = new mysqli($mysql_host, $mysql_user, $mysql_password, $mysql_db);
    if($mysqli->connect_errno){
        echo $mysql_error_msg;
    }
    $userid = -1;
    if(isset($_COOKIE["UID"]))
        $userid = intval($_COOKIE["UID"]);
    if($userid != -1 && $mysqli->query("SELECT `authtoken` FROM `users` WHERE userid='$userid';")->fetch_object()->{"authtoken"} == sha1($_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'])){
        $email = $mysqli->query("SELECT `email` FROM `users` WHERE userid='$userid';")->fetch_object()->{"email"};
    }else{
        setcookie("UID", "", time() - 3600);
        $userid = -1;
        $email = "Гость";
    }
    $mysqli->close();
?>

<html>
    <head>
        <title><?php echo $site_title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"> 
        <meta name="description" content="<?php echo $site_description; ?>">
    </head>
    <body>
        <h1>Здравствуй, <?php echo $email; ?></h1>
        <?php if ($userid > -1) echo "<h2><a href='" . $site_host . "exit'>Выйти</a></h2>" ?>
        <h2><a href="panel">Перейти в панель управления умными фермами</a></h2>
    </body>
</html>

