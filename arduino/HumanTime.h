#pragma once

#include <Arduino.h>
#include "string.h"

class HumanTime
{
public:
struct tstamp
{
    int h = 0;
    int m = 0;
    int s = 0;
};

    tstamp empty;
    bool compare_time(tstamp lastchange, tstamp current, tstamp period);
    bool compare_time(tstamp lastchange, tstamp current);
    bool compare_time(String a, String b);
    void writeAt(tstamp ts, char* a);
    String asStr(tstamp a);
};

bool HumanTime::compare_time(tstamp lastchange, tstamp current, tstamp period)
{
  if(current.s < lastchange.s)
  {
    current.s+=60;
    current.m--;
  }
  if(current.m<lastchange.m)
  {
    current.m+=60;
    current.h--;
  }
  if(current.h<lastchange.h)
  {
    current.h+=24;
  }
  
  if(current.h-lastchange.h >= period.h)
  {
    if(current.m-lastchange.m >= period.m)
    {
        if(current.s-lastchange.s >= period.s)
        return true;
    }
  }
  return false;
}

bool HumanTime::compare_time(tstamp lastchange, tstamp current)
{
  if(current.s < lastchange.s)
  {
    current.s+=60;
    current.m--;
  }
  if(current.m<lastchange.m)
  {
    current.m+=60;
    current.h--;
  }
  if(current.h<lastchange.h)
  {
    current.h+=24;
  }
  
  if(current.h-lastchange.h > 0)
  {
    if(current.m-lastchange.m > 0)
    {
        if(current.s-lastchange.s > 0)
        return true;
    }
  }
  return false;
}

void HumanTime::writeAt(tstamp ts, char* a)
{
    int h = (a[0] - '0') + (a[1] - '0');
    int m = (a[3] - '0') + (a[4] - '0');
    int s = (a[6] - '0') + (a[7] - '0');
    ts.h = h;
    ts.m = m;
    ts.s = s;
}

bool HumanTime::compare_time(String a, String b)
{
    int h = (a[0] - '0') + (a[1] - '0');
    int m = (a[3] - '0') + (a[4] - '0');
    int s = (a[6] - '0') + (a[7] - '0');

    int h1 = (b[0] - '0') + (b[1] - '0');
    int m1 = (b[3] - '0') + (b[4] - '0');
    int s1 = (b[6] - '0') + (b[7] - '0');
    tstamp a1, b1;
    a1.h = h;
    a1.m = m;
    a1.s = s;

    b1.h = h1;
    b1.m = m1;
    b1.s = s1;
    return compare_time(a1, b1);
}

String HumanTime::asStr(tstamp a)
{
    
    return String(a.h) + ":" + String(a.m) + ":" + String(a.s);  
}
