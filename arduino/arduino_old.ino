/*
#include "DHT.h"
#include <FastLED.h>
#include <Adafruit_Sensor.h>
#include <LiquidCrystal.h>
#include "GyverEncoder.h"
#include "menu.h"
#include <EEPROM.h>
#include <iarduino_RTC.h>
#include <Time.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <Wire.h>
#include <DS3231.h>
#include <ArduinoJson.h>
#define menu_items_count 4

//создаем необходимые объекты
String rgba = ""; //ргба строка с цветом
CRGB leds[8]; //светодиодка
DHT dht(40, DHT11);  //датчик температуры-влажности
LiquidCrystal lcd(5, 7, 9, 10, 11, 12); //экран
Encoder selector(16, 15, 14); //энкодер
menu Menu;  //меню
DS3231  rtc(A4, A5); //часы
String inputString = "";
bool stringComplete = false;
//текущий пресет
struct preset
{
  String presetName;
  struct tm* lastchange; //таймстемп пресета
  struct tm* timerFanWork;
  struct tm* timerFitoWork;
  struct tm* timerWaterWork;
 
  struct tm* timerFanOut;
  struct tm* timerFitoOut;
  struct tm* timerWaterOut;
};
preset curStatus; 
//инфа с датчиков
struct info
{
    float h,t; //значение DHT, будет браться из EPROM
    bool relFan, relFito, relWater = 0; // статус реле сейчас
};
info inf;
//время последнего изменения
struct changeTime
{
  struct tm* Fan;
  struct tm* Fito;
  struct tm* Water;
};
changeTime stamps;

void menuSetup() //вторая цифра - строка
{
	Menu.create(); //титульная
  Menu.writeTop("PROJECT CHARON");
  Menu.writeBot("by Limbo");
  lcd.setCursor(0, 0);
  lcd.print(Menu.getTop());
  lcd.setCursor(0, 1);
  lcd.print(Menu.getBot());
  delay(2000);

  Menu.writeTop("STATUS:"); //статус фермы
  Menu.writeBot("Temp    Hum   ");
 
  Menu.create(); // состояние устройств
  Menu.writeTop("PROCESS:");
  Menu.writeBot("FAN WATR LED ");

  Menu.create(); //пресеты или нет
  Menu.writeTop("AUTOMOD:");
  Menu.writeBot("Disable?");

}

void menuUpdate() //обновление экрана меню
{
   lcd.clear();
   lcd.setCursor(0, 0);
   lcd.print(Menu.getTop());
   lcd.setCursor(0, 1);
   lcd.print(Menu.getBot());
}

//48 50 52 реле, применяем изменения
void applyDevices()
{
  if(inf.relFan)
  {
    digitalWrite(48, 1);
  }
  else
  {
    digitalWrite(48, 0);
  }

   if(inf.relFito)
  {
    digitalWrite(50, 1);
  }
  else
  {
    digitalWrite(50, 0);
  }
   if(inf.relWater)
  {
    digitalWrite(52, 1);
  }
  else
  {
    digitalWrite(52, 0);
  }
}

void setup() //сетап ардуино
{

  //сетап портов
  Serial.begin(115200);
  Serial1.begin(9600);

  //сетап пинов под экран
  pinMode(13, OUTPUT); 
  digitalWrite(13,1);
  lcd.begin(16, 2);

  rtc.begin();
  //сетап меню
 // menuSetup();

  //сетап светодиодки
  pinMode(17, OUTPUT);
  FastLED.addLeds<NEOPIXEL, 17>(leds, 8);

  //сетап температуры
  dht.begin();
  pinMode(40, OUTPUT);
  digitalWrite(40, 1);

  //сетап реле
  pinMode(48, OUTPUT);
  pinMode(50, OUTPUT);
  pinMode(52, OUTPUT);
  inputString.reserve(256);

  selector.setType(TYPE1);
}

void readLed() //считывание цвета подсветки
{
  uint32_t rgba1 = strtoul(rgba.c_str(),NULL, 16);
  int a = (rgba1 >> 24) & 0xFF;
  int r = (rgba1 >> 16) & 0xFF;
  int g = (rgba1 >> 8) & 0xFF;
  int b = rgba1 & 0xFF;
  for(int i=0;i< 8;i++)
  {
    leds[i].red = r;
    leds[i].green = g;
    leds[i].blue = b;
  }
  FastLED.show();
}

void getDHT() //получение значений dht в h t и вывод на экран
{
 float hn = dht.readHumidity();
 float tn = dht.readTemperature();
 int h1, t1;
 if(inf.h == 0.0 && inf.t == 0.0)
 {
   inf.h = hn;
   inf.t = tn;
   h1 = hn;
   t1 = tn;
   Menu.getDHT(t1, h1);
   if(Menu.select == Menu.first)
   {
   menuUpdate();
   }
 }
 if(hn != inf.h & tn!= inf.t)
 {
 h1 = hn;
 t1 = tn;
 inf.h = hn;
 inf.t = tn;
  if (isnan(inf.h) || isnan(inf.t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
  }
  else
  {
     Menu.getDHT(t1, h1);
     if(Menu.select == Menu.first)
   {
    menuUpdate();
   }
  }
 }
}

//получаем данные
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;
 
  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
 
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

//отправка данных на ESP. Status и Relay
void sendData()
{
  int hI = inf.h*100;
  int tI = inf.t*100;
  Serial1.println(hI);
  Serial1.println(tI);

  Serial1.println(inf.relFan);
  Serial1.println(inf.relFito);
  Serial1.println(inf.relWater);
}

//если есть триггеры на включение устройств, то включаем
void checkPreset(int array[], int array2[])
{
  for(int i=0;i<3;i++)
  {
    if(inf.t  < array[i] || inf.h < array2[i])
    {
      switch (i){
        case 0: inf.relFan = 1;
        case 1: inf.relFito = 1;
        default: inf.relWater = 1;
      }
    }
  }
}
//проверка поворота энкодера
void checkEncoder()
{
  selector.tick();
 if(selector.isTurn())
 {
   if(selector.isRight())
   {
     Menu.next();
     menuUpdate();
   }
   if(selector.isLeft())
   {
     Menu.prev();
     menuUpdate();
   }
 }
}

//проверка времени, ЕСЛИ НЕ РАБОТАЕТ, ПРОВЕРИТЬ СРАВНЕНИЕ
void presetTime()
{
  //если работает, проверяем время работы, если стоит - время простоя
  if(inf.relFan)
  {
   if(compareTime(1,1))
   {
     inf.relFan =! inf.relFan;
     stamps.Fan->tm_hour = rtc.getTime().hour;
     stamps.Fan->tm_min = rtc.getTime().min;
     stamps.Fan->tm_sec = rtc.getTime().sec;
   }
  }
  else
  {
     if(compareTime(1,0))
   {
     inf.relFan =!inf.relFan;
     stamps.Fan->tm_hour = rtc.getTime().hour;
     stamps.Fan->tm_min = rtc.getTime().min;
     stamps.Fan->tm_sec = rtc.getTime().sec;
   }
  }
  if(inf.relFito)
  {
    if(compareTime(2,1))
   {
     inf.relFito =! inf.relFito;
     stamps.Fito->tm_hour = rtc.getTime().hour;
     stamps.Fito->tm_min = rtc.getTime().min;
     stamps.Fito->tm_sec = rtc.getTime().sec;
   }
  }
  else
  {
     if(compareTime(2,0))
   {
     inf.relFito =! inf.relFito;
     stamps.Fito->tm_hour = rtc.getTime().hour;
     stamps.Fito->tm_min = rtc.getTime().min;
     stamps.Fito->tm_sec = rtc.getTime().sec;
   }
  }
  if(inf.relWater)
  {
    if(compareTime(3,1))
   {
     inf.relWater =! inf.relWater;
     stamps.Water->tm_hour = rtc.getTime().hour;
     stamps.Water->tm_min = rtc.getTime().min;
     stamps.Water->tm_sec = rtc.getTime().sec;
   }
  }
  else
  {
    if(compareTime(3,0))
   {
     inf.relWater =! inf.relWater;
     stamps.Water->tm_hour = rtc.getTime().hour;
     stamps.Water->tm_min = rtc.getTime().min;
     stamps.Water->tm_sec = rtc.getTime().sec;
   }
  }
  //записываем изменение памяти в EPROM
  //EEPROM.put(sizeof(curStatus)+1, stamps);  
}

//считываем данные с порта
void readData() 
{
   while (Serial1.available()) {
    char inChar = (char)Serial1.read();
    if (inChar == '\n')
      stringComplete = true;
    else
      inputString += inChar;    
  }
}

//получение настроек с ESP. Вид - строка, начинающаяся с P. Вентиляция, лампа насос. 
void getData()
{
  //считываем данные с порта
  readData();
  if (stringComplete) {
    Serial.println(inputString);
    String preset_name = getValue(inputString,'<br>',0);
    preset_name.remove(preset_name.length()-3, 3);
    Serial.println(preset_name);
    String fan = getValue(inputString,'<br>',1);
    fan.remove(fan.length()-3, 3);
    Serial.println(fan);
    String lamp = getValue(inputString,'<br>',2);
    lamp.remove(lamp.length()-3, 3);
    Serial.println(lamp);
    String pump = getValue(inputString,'<br>',3);
    pump.remove(pump.length()-3, 3);
    Serial.println(pump);
    String led = getValue(inputString,'<br>',4);
    led.remove(led.length()-3, 3);
    Serial.println(led);
    rgba = led;
    String edit_date = getValue(inputString,'<br>',5);
    edit_date.remove(edit_date.length()-3, 3);
    curStatus.presetName = preset_name;
    int maxT[3], maxH[3];
    sscanf(edit_date.c_str(), "%d-%d-%d %d:%d:%d", &curStatus.lastchange->tm_year,  &curStatus.lastchange->tm_mon,  &curStatus.lastchange->tm_mday,  &curStatus.lastchange->tm_hour,  &curStatus.lastchange->tm_min,  &curStatus.lastchange->tm_sec);
    DynamicJsonDocument JSfan(1024);
    DeserializationError error = deserializeJson(JSfan, fan);
    if (error) {
      Serial.print(F("deserializeJson()1 failed: "));
      Serial.println(error.c_str());
    }
    else {
      int on_time_h = JSfan["on_time_h"];
      int on_time_m = JSfan["on_time_m"];
      int on_time_s = JSfan["on_time_s"];
      int off_time_h = JSfan["off_time_h"];
      int off_time_m = JSfan["off_time_m"];
      int off_time_s = JSfan["off_time_s"];
      int temp = JSfan["temp"];
      int temp_act = JSfan["temp_act"];
      int humidity = JSfan["humidity"];
      int humidity_act = JSfan["humidity_act"];
      int light = JSfan["light"];
      int light_act = JSfan["light_act"];
      
      maxT[0] = temp_act;
      maxH[0] = humidity_act;
    }
    return;
    DynamicJsonDocument JSfito(1024);
    error = deserializeJson(JSfito, lamp);
    if (error) {
      Serial.print(F("deserializeJson()2 failed: "));
      Serial.println(error.c_str());
    }
    else {
      int on_time_h = JSfito["on_time_h"];
      int on_time_m = JSfito["on_time_m"];
      int on_time_s = JSfito["on_time_s"];
      int off_time_h = JSfito["off_time_h"];
      int off_time_m = JSfito["off_time_m"];
      int off_time_s = JSfito["off_time_s"];
      int temp = JSfito["temp"];
      int temp_act = JSfito["temp_act"];
      int humidity = JSfito["humidity"];
      int humidity_act = JSfito["humidity_act"];
      int light = JSfito["light"];
      int light_act = JSfito["light_act"];

      curStatus.timerFitoWork->tm_hour = on_time_h;
      curStatus.timerFitoWork->tm_min = on_time_m;
      curStatus.timerFanWork->tm_sec = on_time_s;

      curStatus.timerFitoOut->tm_hour = off_time_h;
      curStatus.timerFitoOut->tm_min = off_time_m;
      curStatus.timerFitoOut->tm_sec = off_time_s;
      maxT[1] = temp_act;
      maxH[1] = humidity_act;
    }

    DynamicJsonDocument JSpump(1024);
    error = deserializeJson(JSpump, pump);
    if (error) {
      Serial.print(F("deserializeJson()3 failed: "));
      Serial.println(error.c_str());
    }else{
      int on_time_h = JSpump["on_time_h"];
      int on_time_m = JSpump["on_time_m"];
      int on_time_s = JSpump["on_time_s"];
      int off_time_h = JSpump["off_time_h"];
      int off_time_m = JSpump["off_time_m"];
      int off_time_s = JSpump["off_time_s"];
      int temp = JSpump["temp"];
      int temp_act = JSpump["temp_act"];
      int humidity = JSpump["humidity"];
      int humidity_act = JSpump["humidity_act"];
      int light = JSpump["light"];
      int light_act = JSpump["light_act"];

      curStatus.timerWaterWork->tm_hour = on_time_h;
      curStatus.timerWaterWork->tm_min = on_time_m;
      curStatus.timerWaterWork->tm_sec = on_time_s;

      curStatus.timerWaterOut->tm_hour = off_time_h;
      curStatus.timerWaterOut->tm_min = off_time_m;
      curStatus.timerWaterOut->tm_sec = off_time_s;
      maxT[2] = temp_act;
      maxH[2] = humidity_act;
      checkPreset(maxT, maxH);
    }
  inputString = "";
  stringComplete = false;
  }
}

void loop() //цикл контроллера
{
  //записываем в json
   getData();
  //светодиодка
  //readLed();
 //температура-влажность
  getDHT();
 // if(curStatus.presetName!="0")
 // {
  presetTime();
    //}
  //применяем изменения
  applyDevices();
 //энкодер
  checkEncoder();
 //отправка данных на сервер
 // sendData();
}
*/