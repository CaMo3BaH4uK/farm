#include <ArduinoJson.h>
#include "HumanTime.h"
#include <FastLED.h>
#include <Adafruit_Sensor.h>
#include <GyverEncoder.h>
#include <LiquidCrystal_I2C.h>
#include <iarduino_RTC.h>
#include <DS3231.h>
#include "DHT.h"
#include "menu.h"
#include <NewPing.h>
#include "TimerOne.h"
#include <EEPROM.h>  
#include <Wire.h>
#include <EEPROM.h>
//структуры
struct preset{
  String presetName;
  String lastchange = ""; //таймстемп пресета
  HumanTime::tstamp timerFanWork;
  HumanTime::tstamp timerFitoWork;
  HumanTime::tstamp timerWaterWork;
 
  HumanTime::tstamp timerFanOut;
  HumanTime::tstamp timerFitoOut;
  HumanTime::tstamp timerWaterOut;
  int MaxT[3][3];
  int MaxH[3][3];
};
struct info{
    float h,t; //значение DHT, будет браться из EPROM
    bool relFan, relFito, relWater = 0; // статус реле сейчас
    bool manual_control = 1; //ручное управление 0-нет, 1-да
};
struct changeTime{
  HumanTime::tstamp Fan;
  HumanTime::tstamp Fito;
  HumanTime::tstamp Water;
};
//объявление стуктур и классов
preset curStatus; 
changeTime stamps;
info inf;
menu Menu; 
HumanTime x;
int t1, h1 = 0;
//пины устройства
Encoder enc(2, 3, 4);
LiquidCrystal_I2C lcd(0x27, 16, 2); 
CRGB leds[8];
DHT dht(40, DHT11);
DS3231  rtc(A4, A5);
//NewPing sonar(42, 44, MAX_DISTANCE); указать расстояние, далее   unsigned int distance = sonar.ping_cm();

//объявление глобальные переменные
String inputString = "";
String rgba = "";
bool stringComplete = false;

//Software functions
String getValue(String data, char separator, int index){
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;
 
  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
 
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
} //служебная функция, обработка строк  
void readData(){
  while (Serial1.available()) {
    char inChar = (char)Serial1.read();
    if (inChar == '\n')
      stringComplete = true;
    else
      inputString += inChar;    
  }
} //служебная функция, чтение из Serial1
void presetAsStr(){
    Serial.println("Preset name: " + curStatus.presetName);
    Serial.println("last change: " + curStatus.lastchange);

    Serial.println("Fan work's duration: " + x.asStr(curStatus.timerFanWork));
    Serial.println("Fan work's cooldown: " + x.asStr(curStatus.timerFanOut));
    Serial.println("Maximal temperature: " + String(curStatus.MaxT[0][0]));
    Serial.println("Maximal humidity: " + String(curStatus.MaxH[0][0]));

    Serial.println("Fito work's duration: " + x.asStr(curStatus.timerFitoWork));
    Serial.println("Fito work's cooldown: " + x.asStr(curStatus.timerFitoOut));
    Serial.println("Maximal temperature: " + String(curStatus.MaxT[1][0]));
    Serial.println("Maximal humidity: " + String(curStatus.MaxH[1][0]));

    Serial.println("Pump work's duration: " + x.asStr(curStatus.timerWaterWork));
    Serial.println("Pump work's cooldown: " + x.asStr(curStatus.timerWaterOut));
    Serial.println("Maximal temperature: " + String(curStatus.MaxT[2][0]));
    Serial.println("Maximal humidity: " + String(curStatus.MaxH[2][0]));
}  //функция для отладки, вывод пресета в Serial0

//Menu functions
void menuSetup(){
	Menu.create(); //титульная
  Menu.writeTop("PROJECT CHARON");
  Menu.writeBot("by Limbo");
  Menu.show(lcd);

  Menu.writeTop("STATUS:"); //статус фермы
  Menu.writeBot("Temp    Hum   ");
 
  Menu.create(); // состояние устройств
  Menu.writeTop("PROCESS:");
  Menu.writeBot("F   W   L  ");

  Menu.create(); //пресеты или нет
  Menu.writeTop("AUTOMOD:");
  Menu.writeBot("Disable?");

} //создание меню
void menuUpdate() {
   lcd.clear();
   Menu.show(lcd);
} //обновление выбранного экрана

//Hardware functions
void readLed(){
  uint32_t rgba1 = strtoul(rgba.c_str(),NULL, 16);
  int a = (rgba1 >> 24) & 0xFF;
  int r = (rgba1 >> 16) & 0xFF;
  int g = (rgba1 >> 8) & 0xFF;
  int b = rgba1 & 0xFF;
  Serial.println(a);

  for(int i=0;i< 8;i++)
  {
    leds[i].red = r;
    leds[i].green = g;
    leds[i].blue = b;
  }
  FastLED.show();
} //запись светодиодки
void applyDevices(){
  if(inf.relFan)
  {
    digitalWrite(30, 1);
  }
  else
  {
    digitalWrite(30, 0);
  }

   if(inf.relFito)
  {
    digitalWrite(31, 1);
  }
  else
  {
    digitalWrite(31, 0);
  }
   if(inf.relWater)
  {
    digitalWrite(32, 1);
  }
  else
  {
    digitalWrite(32, 0);
  }
  Menu.getDev(inf.relFan, inf.relWater, inf.relFito);
  if(Menu.select == Menu.first->right)
  {
    menuUpdate();
  }
  //readLed();
} //применение изменений устройств

//Info-functions
void getData(){
  readData();
  if (stringComplete) {
    String preset_name = getValue(inputString,'<br>',0);
    preset_name.remove(preset_name.length()-3, 3);
    String fan = getValue(inputString,'<br>',1);
    fan.remove(fan.length()-3, 3);
    String lamp = getValue(inputString,'<br>',2);
    lamp.remove(lamp.length()-3, 3);
    String pump = getValue(inputString,'<br>',3);
    pump.remove(pump.length()-3, 3);
    String led = getValue(inputString,'<br>',4);
    led.remove(led.length()-3, 3);
    String edit_date = getValue(inputString,'<br>',5);
    edit_date.remove(edit_date.length()-3, 3);
    rgba=led;
    if(curStatus.lastchange == edit_date)
    {
      inputString = "";
      stringComplete = false;
      return;
    }
    else
    {
      if(preset_name == "manual")
      {
        inputString = "";
        stringComplete = false;
        EEPROM.put(sizeof(curStatus)+1+sizeof(stamps), inf);
        inf.relFan = 0;
        inf.relFito = 0;
        inf.relWater = 0;
        if(fan == "on")
          inf.relFan = 1;
        if(lamp == "on")
          inf.relFito = 1;
        if(pump == "on")
          inf.relWater = 1;
        inf.manual_control = 1;
        return;
      }
      else
      {
        inf.manual_control = 0;      
        curStatus.presetName = preset_name;
        curStatus.lastchange = edit_date;
        DynamicJsonDocument JSfan(1024);
        DeserializationError error = deserializeJson(JSfan, fan);
        if (error) {
          Serial.print(F("deserializeJson() failed: "));
          Serial.println(error.c_str());
          inputString = "";
          stringComplete = false;
          return;
        }
        DynamicJsonDocument JSfito(1024);
        error = deserializeJson(JSfito, lamp);
        if (error) {
          Serial.print(F("deserializeJson(2) failed: "));
          Serial.println(error.c_str());
          inputString = "";
          stringComplete = false;
          return;
        }
        DynamicJsonDocument JSpump(1024);
        error = deserializeJson(JSpump, pump);
        if (error) {
          Serial.print(F("deserializeJson(3) failed: "));
          Serial.println(error.c_str());
          inputString = "";
          stringComplete = false;
          return;
        }
        curStatus.timerFanWork.h = JSfan["on_time_h"];
        curStatus.timerFanWork.m = JSfan["on_time_m"];
        curStatus.timerFanWork.s = JSfan["on_time_s"];
        curStatus.timerFanOut.h = JSfan["off_time_h"];
        curStatus.timerFanOut.m = JSfan["off_time_m"];
        curStatus.timerFanOut.s = JSfan["off_time_s"];
        curStatus.MaxT[0][0] = JSfan["temp"];
        curStatus.MaxT[0][1] = JSfan["temp_act"];
        curStatus.MaxT[0][2] = JSfan["temp_len"];
        curStatus.MaxH[0][0] = JSfan["humidity"];
        curStatus.MaxH[0][1] = JSfan["humidity_act"];
        curStatus.MaxH[0][2] = JSfan["humidity_len"];

        curStatus.timerFitoWork.h = JSfito["on_time_h"];
        curStatus.timerFitoWork.m = JSfito["on_time_m"];
        curStatus.timerFitoWork.s = JSfito["on_time_s"];
        curStatus.timerFitoOut.h = JSfito["off_time_h"];
        curStatus.timerFitoOut.m = JSfito["off_time_m"];
        curStatus.timerFitoOut.s = JSfito["off_time_s"];
        curStatus.MaxT[1][0] = JSfito["temp"];
        curStatus.MaxT[1][1] = JSfito["temp_act"];
        curStatus.MaxT[1][2] = JSfito["temp_len"];
        curStatus.MaxH[1][0] = JSfito["humidity"];
        curStatus.MaxH[1][1] = JSfito["humidity_act"];
        curStatus.MaxH[1][2] = JSfito["humidity_len"];

        curStatus.timerWaterWork.h = JSpump["on_time_h"];
        curStatus.timerWaterWork.m = JSpump["on_time_m"];
        curStatus.timerWaterWork.s = JSpump["on_time_s"];
        curStatus.timerWaterOut.h = JSpump["off_time_h"];
        curStatus.timerWaterOut.m = JSpump["off_time_m"];
        curStatus.timerWaterOut.s = JSpump["off_time_s"];
        curStatus.MaxT[2][0] = JSpump["temp"];
        curStatus.MaxT[2][1] = JSpump["temp_act"];
        curStatus.MaxT[2][2] = JSpump["temp_len"];
        curStatus.MaxH[2][0] = JSpump["humidity"];
        curStatus.MaxH[2][1] = JSpump["humidity_act"];
        curStatus.MaxH[2][2] = JSpump["humidity_len"];
        EEPROM.put(sizeof(curStatus)+1+sizeof(stamps), inf);
        EEPROM.put(0, curStatus);
        presetAsStr();
        inputString = "";
        stringComplete = false;
      }
    }
  }
} //обработка полученной от ESP информации
void getDHT(){
  inf.h = dht.readHumidity();
  inf.t = dht.readTemperature();
  if (isnan(inf.h) || isnan(inf.t)) {
    Serial.println(F("Failed to read from DHT sensor!"));
  }
  else
  {
    int h2 = inf.h;
    int t2 = inf.t;
    if(h2 == h1 && t2 == t1)
    {
      return;
    }
    else {
    Serial.println(inf.t);
    Serial.println(inf.h);
    t1=t2;
    h1=h2;
    Menu.getDHT(t2, h2);
    if(Menu.select == Menu.first)
    {
      menuUpdate();
    }
    }
  }

} //получение DHT и вывод на экран
void checkTime(){
  HumanTime::tstamp now;
  Serial.println("cool");
  x.writeAt(now, rtc.getTimeStr());
  Serial.println("Lul");
  if(inf.manual_control) {return;}
  Serial.println("wut?");
  if(inf.relFan)
  {
   if(x.compare_time(stamps.Fan, now, curStatus.timerFanWork))
   {
     inf.relFan =!inf.relFan;
     stamps.Fan.h = now.h;
     stamps.Fan.m = now.m;
     stamps.Fan.s = now.s;
   }
  }
  else
  {
     if(x.compare_time(stamps.Fan, now, curStatus.timerFanOut))
   {
     inf.relFan =!inf.relFan;
     stamps.Fan.h = now.h;
     stamps.Fan.m = now.m;
     stamps.Fan.s = now.s;
   }
  }

  if(inf.relFito)
  {
    if(x.compare_time(stamps.Fito, now, curStatus.timerFitoWork))
   {
     inf.relFito =! inf.relFito;
     stamps.Fito.h = now.h;
     stamps.Fito.m = now.m;
     stamps.Fito.s = now.s;
   }
  }
  else
  {
     if(x.compare_time(stamps.Fito, now, curStatus.timerFitoOut))
   {
     inf.relFito =! inf.relFito;
     stamps.Fito.h = now.h;
     stamps.Fito.m = now.m;
     stamps.Fito.s = now.s;
   }
  }

  if(inf.relWater)
  {
    if(x.compare_time(stamps.Water, now, curStatus.timerWaterWork))
   {
     inf.relWater =! inf.relWater;
     stamps.Water.h = now.h;
     stamps.Water.m = now.m;
     stamps.Water.s = now.s;
   }
  }
  else
  {
    if(x.compare_time(stamps.Water, now, curStatus.timerWaterOut))
   {
     inf.relWater =! inf.relWater;
     stamps.Water.h = now.h;
     stamps.Water.m = now.m;
     stamps.Water.s = now.s;
   }
  }
  Serial.println("Pretty good");
  //записываем изменение памяти в EPROM
  EEPROM.put(sizeof(curStatus)+1, stamps);
  Serial.println("lul");
  applyDevices();
} //проверяем, не пора ли изменить статус устройства
void sendData(){
  int hI = inf.h*100;
  int tI = inf.t*100;
  Serial.println(tI);
  Serial.println(hI);
  Serial.println(inf.relFan);
  Serial.println(inf.relFito);
  Serial.println(inf.relWater);
  Serial.println(rgba);
} //отправка данных на есп

//Arduino functions
void setup(){
  //сетап портов
  Serial.begin(115200);
  Serial1.begin(9600);
  inputString.reserve(512);

  //сетап пинов под экран и меню
  lcd.init();
  lcd.backlight();
  menuSetup();

  //сетап энкодера
  enc.setType(TYPE1);
  Timer1.initialize(1000);
  Timer1.attachInterrupt(encTick);

    //сетап светодиодки
  pinMode(17, OUTPUT);
  FastLED.addLeds<NEOPIXEL, 17>(leds, 8);

  //сетап температуры
  dht.begin();

  //сетап реле
  pinMode(48, OUTPUT);
  pinMode(50, OUTPUT);
  pinMode(52, OUTPUT);
  digitalWrite(52, 1);


}

void loop(){                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
  if(enc.isTurn())
  {
    encGet();
  }
  getDHT();
  //getData();
  Serial.println("11");
   checkTime();
  //sendData();
}

//прерывания, чтение энкодера
void encGet()
{
  if (enc.isRight()){
     Menu.next();
     menuUpdate();}
 else if (enc.isLeft()) {
  Menu.prev();
  menuUpdate();}
}
void encTick()
{
  enc.tick();
}