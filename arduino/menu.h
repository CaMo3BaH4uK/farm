#pragma once

#include <Arduino.h>
#include "string.h"
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
class menu
{
private:
struct page
{
    page* left = nullptr;
	page* right = nullptr;
    char data[2][16];
};
public:

    void create();
    void next();
    void prev();
    void writeTop(String a);
    void writeBot(String a);
    void getDHT(int t, int h);
    void show(LiquidCrystal_I2C lcd);
    void getDev(int relFan, int relWater, int relLamp);
    String getTop();
    String getBot();
    
    page* select = nullptr;
    page* first = nullptr;
};
void menu::create()
{
    page* newPage = new page;

    if(first == nullptr)
    {
       newPage->left = select;
       select = newPage;
       first = newPage;
    }
    else
    {
        newPage->left = select;
        select->right = newPage;
        newPage->right = first;
        first->left = newPage;
        select = newPage;
    }
}

void menu::next()
{
    select = select->right;
}

void menu::prev()
{
    select = select->left;
}

void menu::writeTop(String a)
{
    a.toCharArray(select->data[0], a.length()+1);
}

void menu::writeBot(String a)
{
    a.toCharArray(select->data[1], a.length()+1);
}

String menu::getTop()
{
    return String(select->data[0]);
}

String menu::getBot()
{
    return String(select->data[1]);
}
void menu::getDHT(int t, int h)
{
    first->data[1][5] = t/10 + '0';
    first->data[1][6] = t%10 + '0';
    first->data[1][12] = h/10 + '0';
    first->data[1][13] = h%10 + '0';
}
void menu::show(LiquidCrystal_I2C lcd)
{
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print(menu::getTop());
    lcd.setCursor(0, 1);
    lcd.print(menu::getBot());
}
void menu::getDev(int relFan, int relWater, int relLamp)
{
    first->right->data[1][2] = relFan;
    first->right->data[1][6] = relWater;
    first->right->data[1][8] = relLamp;
}